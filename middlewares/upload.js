const multer = require('multer');
const config = require('../config/multer');
const env = process.env.NODE_ENV || 'development';
const upload = multer(config[env]);

module.exports = upload;