const jwt = require('jsonwebtoken');
const User = require('../models').user;
require('dotenv').config();

module.exports = async (req, res, next) => {
  if(req.headers.authorization) {
    try {
      let token = req.headers.authorization; 
      let payload = await jwt.verify(token, process.env.SECRET_KEY);
      
      // Find the user instance and assign to the req.user
      let user = await User.findByPk(payload.id)
      req.user = user;
      next();
    }
  
    catch(err) {
      res.status(401).json({
        status: 'Failed',
        message: err.message
      })
    }

  }else{
      res.status(401).json({
        status: 'Failed',
        message: 'You should login first !!!'
      })

  }
}
