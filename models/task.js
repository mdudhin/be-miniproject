'use strict';
module.exports = (sequelize, DataTypes) => {
  const task = sequelize.define('task', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please input your title'
        },
      }
    },  
    description: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    importance: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    completion: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    due_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    user_id: DataTypes.INTEGER
  }, {});
  task.associate = function(models) {
    // associations can be defined here
    task.belongsTo(models.user,{
      foreignKey: 'user_id'
    }) 
  };
  return task;
};