'use strict';
const bcrypt = require('bcryptjs');


module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: "It must be an email!"
        },
        isLowercase: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [6],
          msg: 'Password should be at least 6 characters'
        }
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },
      beforeCreate: instance => {
        instance.password = bcrypt.hashSync(instance.password, 10)
      }
    }
  });
  user.associate = function(models) {
    // associations can be defined here
    user.hasMany(models.task,{
      foreignKey: 'user_id'
    }),
    user.hasOne(models.profile, {
       foreignKey: 'user_id'
     })
  };
  return user;
};