require('dotenv').config();
const request = require('supertest');
const app = require('../index');
const db = require('../models');
const jwt = require('jsonwebtoken');

const User = require('../models').user;
const Profile = require('../models').profile;
// const { task: Task } = require('../models');



let token;

describe('API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "User", "Task", "Profile" RESTART IDENTITY');
        User.create({
            email: 'hello@mail.com',
            password: '123456'
        })
            .then(user => {
                Profile.create({
                    name: 'hello',
                    user_id: user.dataValues.id
                })
                    .then(profile => {
                        token = jwt.sign({
                            id: user.dataValues.id,
                            email: user.dataValues.email
                        }, process.env.SECRET_KEY);
                        console.log(token)
                        done()
                    })
            })
        
    })
    afterAll(() => {
        db.sequelize.query('TRUNCATE "User", "Task", "Profile" RESTART IDENTITY');
    })


    describe('USERS /user/register', () => {
        test('Should successfully registered new user', done => {
            request(app)
                .post('/api/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    email: "lorem@mail.com",
                    password: "123456",
                    name: "lorem"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('Success');
                    done();
                })
        }),
            test('Failed user register', done => {
                request(app)
                    .post('/api/user/register')
                    .set('Content-Type', 'application/json')
                    .send({
                        email: "lorem",
                        password: "123",
                        name: "lorem"
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(422);
                        expect(res.body.status).toEqual('Failed');
                        done();
                    })
            })
    }),

        describe('USERS /user/login', () => {
            test('Should successfully Login', done => {
                request(app)
                    .post('/api/user/login')
                    .set('Content-Type', 'application/json')
                    .send({
                        email: "hello@mail.com",
                        password: "123456",
                    })
                    .then(res => {
                        token = res.body.data.token
                        expect(res.statusCode).toEqual(201);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            }),
            test('Failed user login', done => {
                request(app)
                    .post('/api/user/login')
                    .set('Content-Type', 'application/json')
                    .send({
                        email: "hello@mail.com",
                        password: "12345",
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(401);
                        expect(res.body.status).toEqual('Failed');
                        done();
                    })
            })
        }),
        describe('PROFILE /profile/:id', () => {
            test('Should successfully get profile', done => {
                request(app)
                    .get('/api/profile/1')
                    .set('authorization', token)
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            }, 30000)
        }),

        describe('PROFILE /profile/:id', () => {
            test('Should successfully update', done => {
                request(app)
                    .put('/api/profile/1')
                    .set('authorization', token)
                    .attach('image', "./uploads/hazard.jpg")
                    .then(res => {

                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            }, 30000)

            test('Failed Update', done => {
                request(app)
                    .put(`/api/profile/1`)
                    .set('authorization', `${token}s`)
                    .set('Content-Type', 'application/json')
                    .send({
                        name: null
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(401);
                        expect(res.body.status).toEqual('Failed');
                        done();
                    })
            })
        }),

        describe('TASK /task', () => {
            test('Should successfully create task', done => {
                request(app)
                    .post('/api/task/')
                    .set('authorization', token)
                    .set('Content-Type', 'application/json')
                    .send({
                        title: 'hello world',
                        due_date: '06-06-2020'
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(201);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            })
            test('Failed create task', done => {
                request(app)
                    .post('/api/task/')
                    .set('authorization', `${token}s`)
                    .set('Content-Type', 'application/json')
                    .send({
                        title: null,

                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(401);
                        expect(res.body.status).toEqual('Failed');
                        done();
                    })
            })
        }),

        describe('TASK /task', () => {
            test('Should successfully Get all task', done => {
                request(app)
                    .get('/api/task/')
                    .set('authorization', token)
                    .set('Content-Type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            })
            test('Should successfully Get all importance task', done => {
                request(app)
                    .get('/api/task/')
                    .set('authorization', token)
                    .set('Content-Type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            })
            test('Should successfully Get all completion task', done => {
                request(app)
                    .get('/api/task/')
                    .set('authorization', token)
                    .set('Content-Type', 'application/json')
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            })

        }),

        describe('TASK /task/:id Update', () => {
            test('Should successfully update task', done => {
                request(app)
                    .put(`/api/task/1`)
                    .set('authorization', token)
                    .set('Content-Type', 'application/json')
                    .send({
                        title: 'hello from the other side',
                        due_date: '06-06-2021'
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            })
            test('Failed to update task', done => {
                request(app)
                    .put(`/api/task/1`)
                    .set('authorization', `${token}s`)
                    .set('Content-Type', 'application/json')
                    .send({
                        title: null
                    })
                    .then(res => {
                        expect(res.statusCode).toEqual(401);
                        expect(res.body.status).toEqual('Failed');
                        done();
                    })
            })
        }),

        describe('TASK /task/:id Delete', () => {
            test('Should successfully delele task', done => {
                request(app)
                    .delete(`/api/task/1`)
                    .set('authorization', token)
                    .then(res => {
                        expect(res.statusCode).toEqual(200);
                        expect(res.body.status).toEqual('Success');
                        done();
                    })
            })

        })

})  