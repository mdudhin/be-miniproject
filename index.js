const dotenv = require('dotenv').config();
const express = require("express");
const app = express();
const router = require('./router.js');
const cors = require('cors');
const morgan = require('morgan');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
 



app.use(cors());

// swagger
app.use('/api-docs/rijal', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


app.use(express.json());

if (process.env.NODE_ENV !== 'test')
  app.use(morgan('dev'))


app.use(express.urlencoded({extended: true}))

app.use('/api', router);

module.exports = app ;